// run all modules ending in ".spec.js" or ".spec.ts" in the (sibling) js directory and below
const unitTests = require.context("../js", true, /\.spec\.[tj]s$/);
unitTests.keys().forEach(unitTests);

const globalTests = require.context(".", true, /\.spec\.[tj]s$/);
globalTests.keys().forEach(globalTests);
