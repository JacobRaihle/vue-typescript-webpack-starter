import {markdown} from "markdown";

type PreventDefault = () => any;

export interface Source {
    value: string;
}

export interface Preview {
    innerHTML: string;
}

export default {
    attachPreviewer($source: Source, $preview: Preview) {
        return (event: {preventDefault: PreventDefault}) => {
            const text = $source.value;
            $preview.innerHTML = markdown.toHTML(text);
            event.preventDefault();
        };
    },
};
