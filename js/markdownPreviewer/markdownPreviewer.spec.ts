import markdownPreviewer from "./markdownPreviewer";

describe("markdownPreviewer", () => {
    describe("attachPreviewer", () => {
        it("renders markdown to the passed preview pane", () => {
            const event = jasmine.createSpyObj(["preventDefault"]);
            const source = {value: ""};
            const preview = {innerHTML: ""};

            const submitHandler = markdownPreviewer.attachPreviewer(source, preview);
            source.value = "Some _example_ **markdown**";
            submitHandler(event);

            expect(preview.innerHTML).toBe("<p>Some <em>example</em> <strong>markdown</strong></p>");
            expect(event.preventDefault).toHaveBeenCalled();
        });
    });
});
