const path = require('path');

module.exports = function(config) {
    config.set({
        basePath: '..',
        frameworks: ['jasmine'],
        files: [
            'spec/allTests.js'
        ],
        browsers: ['jsdom'],
        preprocessors: {
            'spec/allTests.js': ['webpack', 'sourcemap', 'coverage']
        },
        reporters: ['mocha', 'coverage', 'remap-coverage', 'istanbul-threshold'],
        remapOptions: {
            exclude: /spec/
        },
        coverageReporter: {
            type: 'in-memory',
            includeAllSources: true,

        },
        remapCoverageReporter: {
            'text-summary': null,
            html: './dist/coverage/html',
            cobertura: './dist/coverage/cobertura.xml',
            json: './dist/coverage/coverage.json'
        },
        istanbulThresholdReporter: {
            src: './dist/coverage/coverage.json',
            basePath: path.resolve(__dirname, '..'),
            reporters: ['text'],
            thresholds: {
                global: {
                    statements: 85,
                    branches: 85,
                    functions: 85
                },
                each: {
                    statements: 85,
                    branches: 85,
                    functions: 85
                }
            }
        },
        webpack: require('./webpack.coverage.js'),
        webpackServer: {
            noInfo: true
        }
    });
};
