module.exports = function(config) {
    config.set({
        basePath: '..',
        frameworks: ['jasmine'],
        files: [
            'spec/allTests.js'
        ],
        browsers: ['jsdom'],
        preprocessors: {
            'spec/allTests.js': ['webpack', 'sourcemap']
        },
        reporters: ['mocha'],
        webpack: require('./webpack.test.js'),
        webpackServer: {
            noInfo: true
        }
    });
};
