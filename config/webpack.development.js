const path = require('path');
const Merge = require('webpack-merge');
const CommonConfig = require('./webpack.common.js');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = Merge(CommonConfig, {
    mode: 'development',
    entry: './js/index.ts',
    output: {
        path: path.join(__dirname, '../dist/build'),
        filename: 'bundle.js'
    },
    devtool: 'inline-source-map',
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'styles.css'
        })
    ]
});
